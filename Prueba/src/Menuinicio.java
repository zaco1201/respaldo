
import java.util.Scanner;

public class Menuinicio {
    
    public static void main(String[] args) {
        
        Cuenta miCuenta[] = new Cuenta[10];
        miCuenta[0] = new Cuenta();
        miCuenta[1] = new Cuenta();
        miCuenta[2] = new Cuenta();
        miCuenta[3] = new Cuenta();
        miCuenta[4] = new Cuenta();
        miCuenta[5] = new Cuenta();
        miCuenta[6] = new Cuenta();
        miCuenta[7] = new Cuenta();
        miCuenta[8] = new Cuenta();
        miCuenta[9] = new Cuenta();
        Scanner seleccionOpcion = new Scanner(System.in);
        Scanner seleccionTipoUsuario = new Scanner(System.in);
        Scanner crearUsuario = new Scanner(System.in);
        Scanner crearContraseña = new Scanner(System.in);
        Scanner ingresarUsuario = new Scanner(System.in);
        Scanner ingresarContraseña = new Scanner(System.in);
        int contadorCuentas = 0;
        int contadorCuentasIngreso = 0;
        int opcion;
        int tipoUsuario;
        String usuarioNuevo;
        String contraseñaNueva;
        String ingresoUsuario;
        String ingresoContraseña;
        
        System.out.println("¿Que accion desea realizar?");
        System.out.println("...........................");
        System.out.println("1. Registrarse");
        System.out.println("2. Ingresar");
        opcion = seleccionOpcion.nextInt();
        
        while(opcion < 1 || opcion > 2) {
            System.out.println("Opcion no valida");
            System.out.println("................");
            System.out.println("¿Que accion desea realizar?");
            System.out.println("...........................");
            System.out.println("1. Registrarse");
            System.out.println("2. Ingresar");
            opcion = seleccionOpcion.nextInt();
            
        }
            
        while(opcion == 1) {
            System.out.println("¿Como desea registrarse?");
            System.out.println("................");
            System.out.println("1. Cliente");
            System.out.println("2. Cocinero");
            System.out.println("3. Administrador");
            tipoUsuario = seleccionTipoUsuario.nextInt();
            
            while(tipoUsuario < 1 || tipoUsuario > 3) {
                System.out.println("Opcion no valida");
                System.out.println("................");
                System.out.println("¿Como desea registrarse?");
                System.out.println("................");
                System.out.println("1. Cliente");
                System.out.println("2. Cocinero");
                System.out.println("3. Administrador");
                tipoUsuario = seleccionTipoUsuario.nextInt();
                
            }
            
            while(tipoUsuario == 1) {    
                System.out.println("Cree un nombre de usuario:");
                usuarioNuevo = "Cliente." + crearUsuario.next();
                miCuenta[contadorCuentas].setUsuario(usuarioNuevo);
                System.out.println("Cree una contraseña:");
                contraseñaNueva = crearContraseña.next();
                miCuenta[contadorCuentas].setContraseña(contraseñaNueva);
                contadorCuentas = contadorCuentas + 1;
                tipoUsuario = 0;
                System.out.println("Registro Completo!");
                System.out.println("...........................");
                System.out.println("¿Que accion desea realizar?");
                System.out.println("...........................");
                System.out.println("1. Registrarse");
                System.out.println("2. Ingresar");
                opcion = seleccionOpcion.nextInt();
                
            }
        
            while(tipoUsuario == 2) {
                System.out.println("Cree un nombre de usuario:");
                usuarioNuevo = "Cocinero." + crearUsuario.next();
                miCuenta[contadorCuentas].setUsuario(usuarioNuevo);
                System.out.println("Cree una contraseña:");
                contraseñaNueva = crearContraseña.next();
                miCuenta[contadorCuentas].setContraseña(contraseñaNueva);
                contadorCuentas = contadorCuentas + 1;
                tipoUsuario = 0;
                System.out.println("Registro Completo!");
                System.out.println("...........................");
                System.out.println("¿Que accion desea realizar?");
                System.out.println("...........................");
                System.out.println("1. Registrarse");
                System.out.println("2. Ingresar");
                opcion = seleccionOpcion.nextInt();
            }
            
            while(tipoUsuario == 3) {
                System.out.println("Cree un nombre de usuario:");
                usuarioNuevo = "Admin." + crearUsuario.next();
                miCuenta[contadorCuentas].setUsuario(usuarioNuevo);
                System.out.println("Cree una contraseña:");
                contraseñaNueva = crearContraseña.next();
                miCuenta[contadorCuentas].setContraseña(contraseñaNueva);
                contadorCuentas = contadorCuentas + 1;
                tipoUsuario = 0;
                System.out.println("Registro Completo!");
                System.out.println("...........................");
                System.out.println("¿Que accion desea realizar?");
                System.out.println("...........................");
                System.out.println("1. Registrarse");
                System.out.println("2. Ingresar");
                opcion = seleccionOpcion.nextInt();
            }    
            
        }    
        
        while(opcion == 2) {
            System.out.println("Nombre de usuario:");
            ingresoUsuario = ingresarUsuario.next();
            
            while("Cliente." + ingresoUsuario != miCuenta[contadorCuentasIngreso].getUsuario()) {
                System.out.println("Contrseña:");
            }
            
        }
        
    }
    
}