package application;

import java.awt.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controller {
	@FXML
	private Button boton;
	
	@FXML
	private TextField ingresarNombre;
	
	@FXML
	private Label Nombre;
	
	@FXML
	void mostrarPantalla(ActionEvent event) {
		String nombre = ingresarNombre.getText();
        Nombre.setText(nombre);
	}

}
