include C:\Irvine\Irvine32.inc
includelib C:\Irvine\Irvine32.lib
includelib C:\Irvine\Kernel32.lib
includelib C:\Irvine\user32.lib
include C:\Irvine\macros.inc
include ac_atan2.inc
include io_functions.inc
	
.386
.model flat,stdcall
.stack 4096
ExitProcess PROTO, dwExitCode:DWORD

.data
;initialize the variables
x DWORD ?
y DWORD ?
resultado QWORD ?
BUFFER_SIZE = 7777
buffer BYTE BUFFER_SIZE DUP(?)
flag BYTE ?
cont DWORD ?

.code

main PROC

; Clean the register
INVOKE read_values, OFFSET buffer
	xor edi, edi
	xor esi, esi
	xor eax, eax
	xor ecx, ecx
	xor edi, edi
	jmp atoiX
	exit

 INVOKE ExitProcess,0

main ENDP

atoiX:

    xor eax, eax             
	xor esi, esi
	movzx esi, byte PTR buffer + [edi]   
	cmp esi, 45
	jne convertX
	inc edi
     
convertX_N:

	movzx esi, byte PTR buffer + [edi]   
    cmp esi, 44        
    je doneX_N
    sub esi, 48             ; Convert from ASCII to decimal 
    imul eax, 10            
    add eax, esi            
    inc edi                 
    jmp convertX_N


convertX:

    movzx esi, byte PTR buffer + [edi]  
    cmp esi, 44        
    je doneX
    sub esi, 48             ; Convert from ASCII to decimal 
    imul eax, 10           
    add eax, esi            
    inc edi                 
    jmp convertX
 
; Case where x is positive
doneX:

	mov x, eax
	jmp atoiY

; Case where x is negative
doneX_N:

	neg eax
	mov x, eax
	jmp atoiY

; Convert ASCII to Integer
atoiY:

    xor eax, eax              
	xor esi, esi
	inc edi
	movzx esi, byte PTR buffer + [edi]   
	cmp esi, 45
	jne convertY
	inc edi
     
convertY_N:

	movzx esi, byte PTR buffer + [edi]   
    cmp esi, 0ah        
    je doneY_N
	cmp esi, 0dh
	je doneY_N 
    sub esi, 48             ; Convert from ASCII to decimal 
    imul eax, 10           
    add eax, esi            
    inc edi                
    jmp convertY_N

convertY:

    movzx esi, byte PTR buffer + [edi]   
    cmp esi, 0Ah       
    je doneY
	cmp esi, 0dh
	je doneY
    sub esi, 48             ; Convert from ASCII to decimal 
    imul eax, 10           
    add eax, esi            
    inc edi                 
    jmp convertY

; Case where y is positive
doneY:

	mov y, eax
	jmp L1

; Case where y is negative
doneY_N:

	neg eax
	mov y, eax
	jmp L1

L1:

	inc edi
	xor eax, eax
	INVOKE ac_atan2, x, y
	call writeInt
	mov ebx, eax
	jmp intToAscii

L2:

	mWrite <0dh,0ah>
	inc edi
	movzx esi, byte PTR buffer + [edi]
	cmp esi, 46
	je  quit 
	jmp atoiX

intToAscii:

	mov cont, 0
	push edi
	xor ebx,ebx
	xor esi,esi
	xor edx,edx
	xor ecx,ecx
	cmp eax, 0
	jge ToPositiveAscii
	jmp ToNegativeAscii
	
	
ToPositiveAscii:

	push eax
	mov ecx, 10         
	xor bx, bx         
	xor esi, esi

dividePositive:

	xor edx, edx        
	div ecx             
	push dx             
	inc bx              
	test eax, eax       
	jnz dividePositive         ; if not, continue
	mov word ptr cont, bx
	mov cx, bx          
	lea esi, resultado   

nextPositive:

	pop ax
	add al, '0'         ; convert to ASCII
	mov [esi], al      
	inc esi
	loop nextPositive
	mov flag, 1
	INVOKE write_values, flag, resultado, cont
	jmp L2

	
ToNegativeAscii:

	neg eax
	push eax
	mov ecx, 10         
	xor bx, bx          
	xor esi, esi

divideNegative:

	xor edx, edx        
	div ecx             
	push dx             
	inc bx              
	test eax, eax       
	jnz divideNegative          ; if not, continue
	mov word ptr cont, bx
	mov cx, bx         
	lea esi, resultado   

nextNegative:

	pop ax
	add al, '0'         ; convert to ASCII
	mov [esi], al       
	inc esi
	loop nextNegative
	mov flag, 0
	INVOKE write_values, flag, resultado, cont
	jmp L2

	quit:
		exit

END main