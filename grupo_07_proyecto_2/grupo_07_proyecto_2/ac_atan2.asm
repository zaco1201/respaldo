include C:\Irvine\Irvine32.inc
includelib C:\Irvine\Irvine32.lib
includelib C:\Irvine\Kernel32.lib
includelib C:\Irvine\user32.lib

.386
.model flat,stdcall
.stack 4096
ExitProcess PROTO, dwExitCode:DWORD

.data

absX DWORD ?
absY DWORD ?
num DWORD ? 
denom DWORD ? 
denominadorI DWORD ?
PI EQU 102944
PI_M EQU 51472
PI_C EQU 25736
Q3PI_C EQU 77208
PI_M_N EQU -51472
PI_C_N EQU -25736
Q3PI_C_N EQU -77208
OneHundred DD 100
Xpot2 DWORD ? 
Ypot2 DWORD ? 
octant DWORD ?
theta DWORD ?

.code

PUBLIC ac_atan2

ac_atan2 PROC x:DWORD, y:DWORD

x_pot2Label:

	mov eax, x
    mov ebx, eax
    mul ebx
    mov Xpot2, eax
    xor eax, eax
    xor ebx, ebx
    xor edx, edx

y_pot2Label:

    mov eax, y
    mov ebx, eax
    mul ebx
    mov Ypot2, eax
    xor eax, eax
    xor ebx, ebx
    xor edx, edx

abs_xLabel:

    mov eax, x
    notyet_x: 
    neg eax
    jl notyet_x
    mov absX, eax

abs_yLabel:

    mov eax, y
    notyet_y: 
    neg eax
    jl notyet_y
    mov absY, eax

numeradorLabel:

    mov eax, x
    mov ebx, y
    mul ebx
    mov num, eax
    xor eax, eax
    xor ebx, ebx
    xor edx, edx

denominadorLabel:

    mov eax, Xpot2
    mov ebx, Ypot2
    shr ebx, 2
    add eax, ebx
    shr ebx, 3
    add eax, ebx
	shr eax, 15
    mov denom, eax
    xor eax,eax
    xor ebx, ebx

denominador_invertidoLabel:

    mov eax, Ypot2
    mov ebx, Xpot2
    shr ebx, 2
    add eax, ebx
    shr ebx, 3
    add eax, ebx
	shr eax, 15
    mov denominadorI, eax
    xor eax, eax
    xor ebx, ebx

casos_especiales:

	mov ebx, x
	mov ecx, y
	cmp ebx, 0
	je x_equals_0
	cmp ecx, 0
	je y_equals_0
	mov ebx, absX
	mov ecx, absY
	cmp ebx, ecx
	je equal_abs
	xor eax, eax
	xor ebx, ebx
	xor ecx, ecx
	jmp calculo

indefinite:

   ret

x_equals_0:

	cmp ecx, 0
	je indefinite
	cmp ecx, 0
	jg y_greater_than_0
	mov theta, PI_M_N
	mov octant, 0
	jmp done_especial

y_greater_than_0:

	mov theta, PI_M
	mov octant, 0
	jmp done_especial

y_equals_0:

	cmp ebx, 0
	jg x_greater_than_0
	mov theta, PI
	mov octant, 0
	jmp done_especial

x_greater_than_0:

	mov theta, 0
	mov octant, 0
	jmp done_especial

equal_abs:

	mov ebx, x
	mov ecx, y
	mov octant, 0
	cmp ebx, 0
	jg abs_x_greater_than_0
	jmp abs_x_less_than_0
	abs_x_greater_than_0:
	mov theta, 0
	cmp ecx, 0
	jg abs_x_y_greater_than_0 
	mov theta, PI_C_N
	jmp done_especial

abs_x_y_greater_than_0:

	mov theta, PI_C
	jmp done_especial

abs_x_less_than_0:

	mov theta, 0
	cmp ecx, 0
	jl abs_x_y_less_than_0
	mov theta, Q3PI_C
	jmp done_especial

abs_x_y_less_than_0:

	mov theta, Q3PI_C_N
	jmp done_especial

calculo:

	xor edx, edx
	mov ebx, x
	mov ecx, y
	cmp ebx, 0
	jg cuadrant_1_or_4
	jmp cuadrant_2_or_3

cuadrant_1_or_4:

	cmp ecx, 0
	jg cuadrant_1 
	mov ebx, absX
	mov ecx, absY
	cmp ebx, ecx
	jl octant7
	mov octant, 8
	xor edx, edx
	mov eax, num 
	imul OneHundred    
	mov ecx, denom 
	idiv ecx           
	shl edx, 1          
	cmp edx, ecx        
	JL L8               
	inc eax             

L8:

	cdq                
	idiv OneHundred     
		 
notyet_8:

	neg edx
	jl notyet_8
	cmp edx,50
	jl done
	dec eax
	jmp done

octant7:

	mov octant, 7
	mov ecx, PI_M_N
	xor edx,edx
	mov eax, num 
	imul OneHundred    
	mov ebx, denominadorI 
	idiv ebx           
	shl edx, 1          
	cmp edx, ebx        
	JL L7               
	inc eax            

L7:

	cdq                 
	idiv OneHundred     

notyet_7: 
	neg edx
	jl notyet_7
	sub ecx, eax
	mov eax, ecx
	cmp edx,50
	jl  done
	inc eax
    jmp done		

cuadrant_1:

	mov ebx, absX
	mov ecx, absY
	cmp ebx, ecx
	jl octant2
	mov octant, 1
	xor edx, edx
	mov eax, num 
	imul OneHundred    
	mov ecx, denom 
	idiv ecx           
	shl edx, 1         
	cmp edx, ecx        
	JL L1             
	inc eax             

L1:

	cdq                 
	idiv OneHundred    

notyet_1: 

	neg edx
	jl notyet_1
	cmp edx,50
	jl done
	inc eax
	jmp done

octant2:

    mov octant, 2
	mov ecx, PI_M
	xor edx, edx
	mov eax, num 
	imul OneHundred   
	mov ebx, denom  
	idiv ebx            
	shl edx, 1          
	cmp edx, ebx        
	JL L2              
	inc eax             

L2:

	cdq                 
	idiv OneHundred     
	sub ecx, eax
	mov eax, ecx 

notyet_2:

	neg edx
	jl notyet_2
	cmp edx,50
	jl done
	dec eax
	jmp done
    jmp done

cuadrant_2_or_3:

    cmp ecx, 0
	jl cuadrant_3
	mov ebx, absX
	mov ecx, absY
	cmp ebx, ecx
	jl octant3
	mov octant, 4
	mov ecx, PI
	xor edx, edx
	mov eax, num 
	imul OneHundred    
	mov ebx, denom
	idiv ebx            
	shl edx, 1         
	cmp edx, ebx       
	JL L4               
	inc eax           

L4:

	cdq                
	idiv OneHundred     
		 
notyet_4: 

	neg edx
	jl notyet_4
	add eax, ecx
	cmp edx,50
	jl done
	dec eax
	jmp done

octant3:

	mov octant, 3
	mov ecx, PI_M
	xor edx, edx
	mov eax, num
	imul OneHundred    
	mov ebx, denominadorI  
	idiv ebx           
	shl edx, 1         
	cmp edx, ebx        
	JL L3               
	inc eax             

L3:

	cdq                
	idiv OneHundred     
	sub ecx, eax
	mov eax, ecx 

notyet_3:  

	neg edx
	jl notyet_3
	cmp edx, 50
	jl  noinc3
	inc eax
	noinc3:
    jmp done	    	

cuadrant_3: 
 
	mov ebx, absX
	mov ecx, absY
	cmp ebx, ecx
	jl octant6
	mov octant, 5
	mov ecx, PI
	mov ecx, PI
	xor edx, edx
	mov eax, num 
	imul OneHundred    
	mov ebx, denom 
	idiv ebx            
	shl edx, 1          
	cmp edx, ebx       
	JL L5              
	inc eax            

L5:

	cdq                
	idiv OneHundred     
		 
notyet_5:

	neg edx
	jl notyet_5	
	sub eax, ecx
	cmp edx,50
	jl done
	inc eax
	jmp done

octant6:

	mov octant, 6
	mov ecx, PI_M_N
	xor edx,edx	
	mov eax, num 
	imul OneHundred  
	mov ebx, denominadorI  
	idiv ebx            
	shl edx, 1          
	cmp edx, ebx       
	JL L6              
	inc eax            

L6:

	cdq                
	idiv OneHundred 
	
notyet_6: 

	neg edx
	jl notyet_6
	sub ecx, eax
	mov eax, ecx
	cmp edx,50
	jl  done
	dec eax
    jmp done	

done_especial:

	mov eax, theta
	ret

done:

	mov theta, eax
	ret

ac_atan2 ENDP

END