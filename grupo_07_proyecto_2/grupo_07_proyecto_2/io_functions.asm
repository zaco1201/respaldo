include C:\Irvine\Irvine32.inc
includelib C:\Irvine\Irvine32.lib
includelib C:\Irvine\Kernel32.lib
includelib C:\Irvine\user32.lib
include C:\Irvine\macros.inc
include ac_atan2.inc

.386
.model flat,stdcall
.stack 4096
ExitProcess PROTO, dwExitCode:DWORD

	.data
	x DWORD ?
	y DWORD ?
	BUFFER_SIZE = 7777
	lineOut	BYTE "123",0
	negative    byte "-", 0
	inFilename  byte        "input.txt", 0
	lineFeed db " ",13,10,0
	fileHandle  HANDLE ?
	outFilename byte		"output_data.txt", 0
	bytes Dword ?

.code

PUBLIC read_values
PUBLIC write_values

read_values PROC, bufferOFFSET:DWORD

    mov    edx,OFFSET inFilename 
    call   OpenInputFile
    mov    fileHandle,eax

file_ok:

    mov    edx,bufferOffset
    mov    ecx,BUFFER_SIZE
    call    ReadFromFile

close_file:

    mov    eax,fileHandle
    call    CloseFile

quit:

    ret

read_values ENDP

write_values PROC flag:BYTE, resultado:QWORD, cont:DWORD
		
		cmp flag, 0
		jg write_positive
		jmp write_negative

write_positive:

		INVOKE CreateFile,
		ADDR outFilename, GENERIC_WRITE, DO_NOT_SHARE, NULL,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0
		mov fileHandle,eax			
		INVOKE SetFilePointer,
		fileHandle,0,0,FILE_END
		mov eax, fileHandle   
		lea edx, resultado
	    mov ecx, cont
	    call WriteToFile
		mov eax, fileHandle  
		lea edx, lineFeed
	    mov ecx, 3
	    call WriteToFile
		INVOKE CloseHandle, fileHandle
		jmp clear_res
			
write_negative:

		INVOKE CreateFile,
		ADDR outFilename, GENERIC_WRITE, DO_NOT_SHARE, NULL,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0
		mov fileHandle,eax			
		INVOKE SetFilePointer,
		fileHandle,0,0,FILE_END
		mov eax, fileHandle
		lea edx, negative
	    mov ecx, 1
	    call WriteToFile
		mov eax, fileHandle  
		lea edx, resultado
	    mov ecx, cont
	    call WriteToFile
		mov eax, fileHandle  
		lea edx, lineFeed
	    mov ecx, 3
	    call WriteToFile
		INVOKE CloseHandle, fileHandle
		jmp clear_res

clear_res:

		pop eax
		mov ecx, 10         
		xor bx, bx          
		xor esi, esi

Di:

		xor edx, edx
		div ecx         
		push dx          
		inc bx              
		test eax, eax     
		jnz Di  
		mov cx, bx        
		lea esi, resultado 
		next_digit_0:
		pop ax
		mov al, 0      
		mov [esi], al    
		inc esi
		loop next_digit_0
		ret

quit:
			 	
write_values ENDP

END