
#! /usr/bin/env python3
'''
    Este archivo permite extraer datos desde el sitio web de Open Street Map
    utilizando el API de overpas-turbo.eu mediante su módulo de python.

    El resultado es una lista de nodos con el siguiente formato

      [ [id, lat, lon, [[tag1, value1], [tag2, value2], ... ],
      [id, lat, lon, [[tag1, value1], [tag2, value2], ... ],
      [id, lat, lon, [[tag1, value1], [tag2, value2], ... ],
      [id, lat, lon, [[tag1, value1], [tag2, value2], ... ],
      [id, lat, lon, [[tag1, value1], [tag2, value2], ... ] ]

      almacenada en un archivo utilizando el modulo de python denominado pickle
'''
import pandas as pd
import pickle
import overpy

bbox_alajuela_centro = "(10.002620516987,-84.228444099426,10.028991571877,-84.18737411499)"


def _extraer_datos_osm(bbox, nombre_archivo_salida):
    '''
    Extrae datos del sitio Open Street Maps y lo almacena en un archivo
    utilizando la biblioteca pickle

    E: bbox -> Se refiere al concepto de "bounding box" el cuál debe ser un
                texto (latitud del lado sur, longitud del lado oeste,
                        latitud del lado norte, longitud del lado este)

                por ejemplo:
                  "(10.0105024751,-84.2196464539,10.021701711,-84.2077159882)"

                el cuál es el bounding box para Alajuela

    S: ninguna.
    R: bbox y nombre_archivo_salida deben ser textos
    '''

    # Inicializa el módulo de consulta.
    api = overpy.Overpass()

    # extrae la información de los basureros desde OSM
    result = api.query(""" node
                              ['amenity'~'waste_disposal|waste_basket|recycling']""" + bbox + """;
                           out;
                       """)
    nodos = []

    # recorre la lista de nodos que retornó el API de overpass
    for node in result.nodes:

        tags = []
        for tag in node.tags:
            tags += [[tag, node.tags[tag]]]

        nodo = [node.id, float(node.lat), float(node.lon)] + [tags]
        # debug
        # print(nodo)
        nodos += [nodo]

    # Abre el archivo donde se van a guardar las cosas.
    archivo_hidrantes = open(nombre_archivo_salida+".pkl", 'wb')

    # Se guardan las cosas utilizando el módulo pickle
    pickle.dump(nodos, archivo_hidrantes)
    # debug
    # print(nodos)
    pass


def obtener_lista(nombre_archivo, actualizar=False):
    '''Lee los contenidos de un archivo (guardado previamente usando pickle) y
    los retorna.

    Entradas:
    :nombre_archivo: una hilera (str) con el nombre del archivo a leer
    :actualizar: si el valor es verdadero descarga los datos desde OSM

    Salidas:
    Ya que ésta función fue creada específicamente para trabajar con una lista
    de elementos, entonces la lista retornada tendrá el siguiente formato:

      [ [id, lat, lon, [[llave1, valor1], [llave2, valor2], ... ],
      [id, lat, lon, [[llave1, valor1], [llave2, valor2], ... ],
      [id, lat, lon, [[llave1, valor1], [llave2, valor2], ... ],
      [id, lat, lon, [[llave1, valor1], [llave2, valor2], ... ],
      [id, lat, lon, [[llave1, valor1], [llave2, valor2], ... ] ]
    '''
    if actualizar:
        _extraer_datos_osm(bbox_alajuela_centro, nombre_archivo)

    # abre y carga el archivo con la información de los elementos descargados
    archivo_pkl = open(nombre_archivo+'.pkl', 'rb')
    elementos = pickle.load(archivo_pkl)
    return elementos


# Llamada de ejemplo
# obtener_lista("arcihvo_datos", actualizar=True
lista1=obtener_lista("acequia_grande")
lista2=obtener_lista("ciruelas")
lista3=obtener_lista("meza")
lista4=obtener_lista("parque_calián_vargas")
lista5=obtener_lista("parque_central")
lista6=obtener_lista("villa_hermosa")
lista_completa_proyecto=lista1+lista2+lista3+lista4+lista5+lista6+lista6



#Opciones del menú Jose
def opciones():
    print ("\n")
    print ("        BIENVENIDO AL MENÚ PRINCIPAL")
    print (" 1. Listar información de elementos del mapa.")
    print (" 2. Buscar un elemento por el identificador")
    print (" 3. Ver lista de llaves asuciadas a los basureros")
    print (" 4. Visualizar información de zonas pandas")
    print (" 5. Salir")
    print ("\n")
 
def menu():
    opciones()
    opcion=input("¿Qué opción desea? ")
    if opcion == "1":
       print ("------------Elementos listados------------")
       ordenar_lista(lista_completa_proyecto,0)
       menu()
    
    if opcion=="2":
        identificador=input("digite el Id del basurero que desea encontrar ")
        return buscar_elemento(identificador)
        menu()
    
        
    if opcion == "3":
        etiquetas_sin_repetir()
        menu()
    
    if opcion == "4":
        informacion_de_zonas_pandas()
        menu()

    if opcion=="5":
        print ("Hasta luego, vuelva pronto")
     

#opcion 1 Sebastian
        
def ordenar_lista(lista,indice):
    if len(lista)==indice:
        pass

    else:
        buscar_elemento=lista[indice]
        ordenar_lista_aux(buscar_elemento)
        restante= (len(lista)-indice)-1
        print ("Faltan",restante, "elementos por mostrar")
        input("Presione la tecla enter para continuar ")
        ordenar_lista(lista,indice + 1)

def ordenar_lista_aux(basureros):
    etiquetas=basureros[3]
    print("========================================")
    print("Id",basureros[0],"Lat",basureros[1],"Lon",basureros[2])
    print("Etiquetas")
    ordenar_etiquetas(etiquetas,0,len(etiquetas))
   
def ordenar_etiquetas(etiquetas,indice,fin):
    if indice==fin:
        pass
    else:
        sub_etiquetas=etiquetas[indice]
        print("     ",sub_etiquetas[0],"-",sub_etiquetas[1])
        return ordenar_etiquetas(etiquetas,indice + 1,fin)


#opcion 2 Raychell

def buscar_identificador(id_usuario):
    lista=obtener_lista("basureros")
    return buscar_aux(lista,id_usuario, 0, len(lista))

def buscar_aux (lista, id_usuario ,indice, largo):
    if indice == largo:
        return ("El identificador digitado no existe")
    
    sublista_basureros = lista[indice]
    identificador=sublista_basureros[0]
           
    if  identificador  ==  int(id_usuario):
        return ordenar_lista_aux(sublista_basureros)

    else:
         return buscar_aux (lista,id_usuario, indice+1, largo)

#opcion 3 Fabiola

def etiquetas_sin_repetir():
    """
    Esta función obtiene los elementos guardados en .pkl de basureros
"""
    lista=lista_completa_proyecto
    lista_etiquetas=etiquetas_sin_repetir_aux(lista,0,0,[])
    
def etiquetas_sin_repetir_aux (lista,indice_lista, indice_etiquetas, resultado):
    """La funcion guarda en una lista vacia la llaves de todos los elementos sin que se
    que se repitan.
    Entrada: Se recibe por parametros la lista con todos los elementos, indice de lista para
    recorrer las lista en su totalidad, indice etiqueta sirve para trabajar con la lista
    reducida y resultado en donde se guardan las llaves"""
    if indice_lista == len(lista):
        print("\nLas siguientes son las llaves encontradas en los siguientes elementos de basureros descargados:\n")
        return imprimir_lista_llaves(resultado, 0)
    sublista = lista[indice_lista]
    lista_peque= sublista[3]
    if indice_etiquetas == len(lista_peque):
        return etiquetas_sin_repetir_aux(lista,indice_lista+1,0,resultado)
    else:
        etiqueta = lista_peque[indice_etiquetas]
        llave = etiqueta[0]
        if llave in resultado:
            return etiquetas_sin_repetir_aux(lista, indice_lista, indice_etiquetas+1, resultado) 
        else:
            resultado= resultado+[llave]
            return etiquetas_sin_repetir_aux(lista, indice_lista,indice_etiquetas+1,resultado)
    
def imprimir_lista_llaves(lista, indice):
    """
        Esta función imprime los elementos de la lista uno por uno hacia abajo
        Entrada: lista corresponde a todas las llaves, indice 0 para recorrer la lisa
        Salida:  Etiqueta
                 Etiqueta
                   ..."""
    if indice==len(lista):
        pass
    else:
        print("\t", lista[indice])
        return  imprimir_lista_llaves(lista, indice+1)

#Opcion de imprimir la informacion con Pandas
    
def informacion_de_zonas_pandas():
    #En esta funcion imprime la informacion de las zonas seleccionas
    """
    Informacion base:
    Acequia Grande corresponde a zona1
    """
    listas_zonas={"Barrio":["Acequia Grande","Ciruelas","Meza","Parque Calián Vargas","Parque central","Villa Hermosa"],"Cantidad Total":[2,7,2,3,7,5],"Cantidad reciclaje":[0,3,2,0,2,3]}
    datos=pd.DataFrame(listas_zonas)
    print("\n                  Tabla con los datos de las zonas")
    print(datos)
    print("\n")

menu()
