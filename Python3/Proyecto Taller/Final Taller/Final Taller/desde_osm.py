#! /usr/bin/env python3
'''
    Este archivo permite extraer datos desde el sitio web de Open Street Map
    utilizando el API de overpas-turbo.eu mediante su módulo de python.

    El resultado es una lista de nodos con el siguiente formato

      [ [id, lat, lon, [[tag1, value1], [tag2, value2], ... ],
      [id, lat, lon, [[tag1, value1], [tag2, value2], ... ],
      [id, lat, lon, [[tag1, value1], [tag2, value2], ... ],
      [id, lat, lon, [[tag1, value1], [tag2, value2], ... ],
      [id, lat, lon, [[tag1, value1], [tag2, value2], ... ] ]

      almacenada en un archivo utilizando el modulo de python denominado pickle
'''

import pickle
import overpy

bbox_alajuela_centro = "(10.002620516987,-84.228444099426,10.028991571877,-84.18737411499)"


def _extraer_datos_osm(bbox, nombre_archivo_salida):
    '''
    Extrae datos del sitio Open Street Maps y lo almacena en un archivo
    utilizando la biblioteca pickle

    E: bbox -> Se refiere al concepto de "bounding box" el cuál debe ser un
                texto (latitud del lado sur, longitud del lado oeste,
                        latitud del lado norte, longitud del lado este)

                por ejemplo:
                  "(10.0105024751,-84.2196464539,10.021701711,-84.2077159882)"

                el cuál es el bounding box para Alajuela

    S: ninguna.
    R: bbox y nombre_archivo_salida deben ser textos
    '''

    # Inicializa el módulo de consulta.
    api = overpy.Overpass()

    # extrae la información de los basureros desde OSM
    result = api.query(""" node
                              ['amenity'~'waste_disposal|waste_basket|recycling']""" + bbox + """;
                           out;
                       """)
    nodos = []

    # recorre la lista de nodos que retornó el API de overpass
    for node in result.nodes:

        tags = []
        for tag in node.tags:
            tags += [[tag, node.tags[tag]]]

        nodo = [node.id, float(node.lat), float(node.lon)] + [tags]
        # debug
        # print(nodo)
        nodos += [nodo]

    # Abre el archivo donde se van a guardar las cosas.
    archivo_hidrantes = open(nombre_archivo_salida+".pkl", 'wb')

    # Se guardan las cosas utilizando el módulo pickle
    pickle.dump(nodos, archivo_hidrantes)
    # debug
    # print(nodos)
    pass


def obtener_lista(nombre_archivo, actualizar=False):
    '''Lee los contenidos de un archivo (guardado previamente usando pickle) y
    los retorna.

    Entradas:
    :nombre_archivo: una hilera (str) con el nombre del archivo a leer
    :actualizar: si el valor es verdadero descarga los datos desde OSM

    Salidas:
    Ya que ésta función fue creada específicamente para trabajar con una lista
    de elementos, entonces la lista retornada tendrá el siguiente formato:

      [ [id, lat, lon, [[llave1, valor1], [llave2, valor2], ... ],
      [id, lat, lon, [[llave1, valor1], [llave2, valor2], ... ],
      [id, lat, lon, [[llave1, valor1], [llave2, valor2], ... ],
      [id, lat, lon, [[llave1, valor1], [llave2, valor2], ... ],
      [id, lat, lon, [[llave1, valor1], [llave2, valor2], ... ] ]
    '''
    if actualizar:
        _extraer_datos_osm(bbox_alajuela_centro, nombre_archivo)

    # abre y carga el archivo con la información de los elementos descargados
    archivo_pkl = open(nombre_archivo+'.pkl', 'rb')
    elementos = pickle.load(archivo_pkl)
    return elementos


# Llamada de ejemplo
# obtener_lista("arcihvo_datos", actualizar=True
lista1=obtener_lista("acequia_grande")
lista2=obtener_lista("ciruelas")
lista3=obtener_lista("meza")
lista4=obtener_lista("parque_calián_vargas")
lista5=obtener_lista("parque_central")
lista6=obtener_lista("villa_hermosa")
lista_completa_proyecto=lista1+lista2+lista3+lista4+lista5+lista6+lista6
print(lista_completa_proyecto)
