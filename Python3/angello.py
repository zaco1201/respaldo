def multiplica(multiplicando,multiplicador):
    #starting values
    SHL,i,suma,resultado=0,0,0,multiplicador
    #Remove all multiples (2 ^ n) from the multiplier.
    #PD: enter decimals, exit binaries.
    while multiplicador != 0: #mientras el segundo numero no sea 0 porque la respuesta seria 0
        while resultado != 1:
            resultado = resultado // 2
            SHL +=1
        #If it finds a multiple, it moves the value with SHL and autosum.
        suma += multiplicando<<SHL
        #I take the remainder and keep looking for multiples of type 2 ^ n
        multiplicador = multiplicador - 2**SHL
        resultado=multiplicador
        i+=1
        #restart the SHL
        SHL = 0
    #I print the residue in binary form.
    print((bin(suma)[2:]).zfill(16))
#Call
multiplica(0,5)
