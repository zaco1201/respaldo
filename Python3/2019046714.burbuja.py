#algoritmo burbuja]
lista=[42,1,9,35,66,8,22]
def algoritmo_burbuja(lista):
    if type(lista) is not list:
        return "error01"
    else:
      return algoritmo_aux(lista,0,len(lista) -1)

def algoritmo_aux(lista,indice,j):
    indice_reset=0
    if j==0:
        return lista
    if indice==j:
        return algoritmo_aux(lista,indice_reset,j - 1)
    if lista[indice]>lista[indice + 1]: 
        temporal=lista[indice]
        lista[indice]=lista[indice + 1]
        lista[indice + 1]=temporal
        return algoritmo_aux(lista,indice + 1,j)
    else:
        return algoritmo_aux(lista,indice + 1,j)


        
