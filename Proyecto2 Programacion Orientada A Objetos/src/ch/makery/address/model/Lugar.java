package ch.makery.address.model;


public class Lugar {
	
	private String Provincia;
	private String Canton;
	private String Distrito;
	
	public Lugar()
	{
	}
	
	public Lugar(String Provincia,String Canton,String Distrito) 
	{
		this.Provincia = Provincia;
		this.Canton = Canton;
		this.Distrito = Distrito;
	}

	public String getProvincia() {
		return Provincia;
	}

	public void setProvincia(String provincia) {
		Provincia = provincia;
	}

	public String getCanton() {
		return Canton;
	}

	public void setCanton(String canton) {
		Canton = canton;
	}

	public String getDistrito() {
		return Distrito;
	}

	public void setDistrito(String distrito) {
		Distrito = distrito;
	}
	
	public String toString() 
	{
		return Provincia + ", " + Canton + ", " + Distrito;
	}
	

}
