package ch.makery.address.model;

public class CasoCovid {
	
	private String Pais;
	private String Fecha;
	private int Casos;
	private int Activos;
	private int Muertes;
	private int Recuperados;
	
	public CasoCovid(String pais, String fecha, int casos, int muertes, int recuperados) {
		Pais = pais;
		Fecha = fecha;
		Casos = casos;
		this.Activos = (casos-(muertes+recuperados));
		Muertes = muertes;
		Recuperados = recuperados;
	}	
	
	public String getPais() {
		return Pais;
	}
	
	public void setPais(String pais) {
		Pais = pais;
	}
	
	public String getFecha() {
		return Fecha;
	}
	
	public void setFecha(String fecha) {
		Fecha = fecha;
	}
	
	public int getCasos() {
		return Casos;
	}
	
	public void setCasos(int casos) {
		Casos = casos;
	}
	
	public int getMuertes() {
		return Muertes;
	}
	
	public void setMuertes(int muertes) {
		Muertes = muertes;
	}
	
	public int getRecuperados() {
		return Recuperados;
	}
	
	public void setRecuperados(int recuperados) {
		Recuperados = recuperados;
	}

	public int getActivos() {
		return Activos;
	}

	public void setActivos(int activos) {
		Activos = activos;
	}


}
