package ch.makery.address.model;


public class Usuario {
	
	
	private String Nombre;
	private String Contrasena;
	private Lugar Lugar;
	private int Placa;
	private Alerta Alerta;
	

	public Usuario() 
	{
	}
	
	
	public Usuario(String Nombre,String Contrasena,Lugar Lugar,int Placa,Alerta Alerta) 
	{
		this.Nombre= Nombre;
		this.Contrasena = Contrasena;
		this.Lugar = Lugar;
		this.Placa = Placa;
		this.Alerta = Alerta;
	}
	

	public String getNombre() {
		return Nombre;
	}
	public void setNombre(String Nombre) {
		this.Nombre = Nombre;
	}
	
	public String getContrasena() {
		return Contrasena;
	}
	public void setContrasena(String Contrasena) {
		this.Contrasena = Contrasena;
	}
	
	
	public int getPlaca() {
		return Placa;
	}
	public void setPlaca(int Placa) {
		this.Placa = Placa;
	}
	
	public Lugar getLugar() {
		return Lugar;
	}
	public void setLugar(Lugar lugar) {
		this.Lugar = lugar;
	}


	public Alerta getAlerta() {
		return Alerta;
	}


	public void setAlerta(Alerta alerta) {
		Alerta = alerta;
	}
	
	
}
