package ch.makery.address;


import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import ch.makery.address.model.Alerta;
import ch.makery.address.model.CasoCovid;
import ch.makery.address.model.Lugar;
import ch.makery.address.model.Usuario;


public class JSONParser {
	
	private File fileUsuarios;
	private File fileAlertas;
	
	private ObjectMapper mapperUsuarios;
	private ObjectMapper mapperAlertas;
	private ObjectMapper mapperCasos;
	
	
	private JsonNode nodoUsuarios;
	private JsonNode nodoAlertas;
	private JsonNode nodoCasos;
	
	private void inicializarUsuarios() throws JsonProcessingException, IOException {
		File tempUsuarios = new File("data/users.json");
		this.fileUsuarios = tempUsuarios;
		this.mapperUsuarios = new ObjectMapper();
		this.nodoUsuarios = mapperUsuarios.readTree(fileUsuarios);
	}
	
	private void inicializarAlertas() throws JsonProcessingException, IOException {
		File tempAlertas = new File("data/alertas.json");
		this.fileAlertas = tempAlertas;
		this.mapperAlertas = new ObjectMapper();
		this.nodoAlertas = mapperAlertas.readTree(fileAlertas);
	}
	
	private void inicializarCasos() throws JsonProcessingException, IOException {
		URL url = new URL("https://covid19-api.org/api/timeline/CR");
		this.mapperCasos = new ObjectMapper();
		this.nodoCasos = mapperCasos.readTree(url);
	}
	
	
	public JSONParser() {
		try {
			inicializarUsuarios();
			inicializarAlertas();
			inicializarCasos();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	
	
		public CasosCovid cargarCasos() {
			CasosCovid Casos = new CasosCovid();
			
			
			ArrayNode arregloCasos = (ArrayNode) nodoCasos;
	          if (arregloCasos != null) {
	              for(int i=0; i<50;i++) {
	                  JsonNode caso = arregloCasos.get(i);
	                  int casos = caso.get("cases").asInt();
	                  String fecha =caso.get("last_update").asText();
	                  int muertes = caso.get("deaths").asInt();
	                  int recuperados = caso.get("recovered").asInt();
	                  String pais = caso.get("country").asText();
	                  CasoCovid cso = new CasoCovid(pais,fecha,casos,muertes,recuperados);
	                  Casos.add(cso);
	              }
	          }
	          return Casos;		
		}

		//Carga los usuarios desde un json, crea objetos Usuario y los agrega a un ArrayNode de Usuarios
		public Usuarios cargarUsuarios() {
			
			Usuarios usuarios = new Usuarios();
			
			ArrayNode arregloUsuarios = (ArrayNode) nodoUsuarios.get("usuarios");
			if(arregloUsuarios != null) {
				for(int i=0; i<arregloUsuarios.size();i++) {
					JsonNode usuario = arregloUsuarios.get(i);
					String nombre = usuario.get("nombre").asText();
					String contrasena = usuario.get("contrasena").asText();
					
					//codigo para leer el jsonnode lugar y hacerlo un objeto
					JsonNode nodoLugar = arregloUsuarios.get(i).get("lugar");
					String provincia = nodoLugar.get("provincia").asText();
					String canton = nodoLugar.get("canton").asText();
					String distrito = nodoLugar.get("distrito").asText();
					
					Lugar lugar = new Lugar(provincia,canton,distrito);
					
					
					int placa = usuario.get("placa").asInt();
					
					
					//codigo para leer el jsonnode alerta y hacerlo objeto
					JsonNode nodoAlerta = arregloUsuarios.get(i).get("alerta");
					String tipoAlerta = nodoAlerta.get("tipoAlerta").asText();
					
					ArrayList<String> establecimientos = new ArrayList<String>();					
					int recorrerEstablecimientos = 0;
					while(recorrerEstablecimientos < nodoAlerta.get("establecimientos").size()) {
						establecimientos.add(nodoAlerta.get("establecimientos").get(recorrerEstablecimientos).asText());
						recorrerEstablecimientos += 1;
					}
					
					ArrayList<String> restricciones = new ArrayList<String>();
					int recorrerRestricciones = 0;
					while(recorrerRestricciones < nodoAlerta.get("restriccionVehicular").size()) {
						restricciones.add(nodoAlerta.get("restriccionVehicular").get(recorrerRestricciones).asText());
						recorrerRestricciones += 1;
					}
					
					Alerta alerta = new Alerta(tipoAlerta,establecimientos,restricciones);
					
					//crea un usuario y lo agrega en el arraylist de usuarios
					
					Usuario usr = new Usuario(nombre, contrasena,lugar,placa,alerta);
					usuarios.add(usr);
					}
				}
			return usuarios;
			}
		
		
		//Busca un usuario y devuelve un i segun el lugar donde esta, para definir una Sesion a la hora de iniciar sesion...
		public int buscarUsuarios(String Usuario, String Contrasena) {		
			ArrayNode arregloUsuarios = (ArrayNode) nodoUsuarios.get("usuarios");
			if(arregloUsuarios != null) {
				int i = 0;
				while (i < arregloUsuarios.size()) {
					JsonNode usuario = arregloUsuarios.get(i);
					String nombre = usuario.get("nombre").asText();
					String contrasena = usuario.get("contrasena").asText();
					if (nombre.equals(Usuario) & contrasena.equals(Contrasena)) {
						return i;
					}
					else {
						i+=1;
					}
				}
			}
			return -1;
		}
		
		
		//Crea un objeto alerta, para esto el indiceTipo puede ser 0 o 1, en caso de naranja o amarillo respectivamente,
		//ademas recibe la placa para poder determinar la circulacion, esto tambien va dentro de la clase alerta.
		public Alerta obtenerAlerta(int indiceTipo,int Placa) {
			int valorPlaca = Placa%10;
			switch(valorPlaca) 
				{
				case 1:
				case 2:{
					valorPlaca = 0;
					break;
				}
				case 3:
				case 4:{
					valorPlaca = 1;
					break;
				}
				case 5:
				case 6:{
					valorPlaca = 2;
					break;
				}
				case 7:
				case 8:{
					valorPlaca = 3;
					break;
				}
				case 9:
				case 0:{
					valorPlaca = 4;
					break;
				}
				}
			//carga el nodo
			ArrayNode arregloAlertas = (ArrayNode) nodoAlertas.get("alerta");
			JsonNode alerta = arregloAlertas.get(indiceTipo);
			
			//obtiene el tipo de alerta
			String tipo = alerta.get("tipoAlerta").asText();
			
			
			//lee el arraylist de establecimientos uno a uno, y crea un arraylist que es necesario para crear el objeto Alerta
			ArrayList<String> establecimientos = new ArrayList<String>();
			int indice = 0;
			while(indice < alerta.get("establecimientos").size()) {
				String establecimiento = alerta.get("establecimientos").get(indice).asText();
				establecimientos.add(establecimiento);
				indice+=1;
			}
			
			
			//lee el arraylist de restricciones vehiculares uno a uno y crea un arraylist que es necesario para crear el objeto Alerta.
			ArrayList <String> restricciones = new ArrayList<String>();
			String restriccionSemana = alerta.get("restriccionSemana").get(valorPlaca).asText();
			restricciones.add(restriccionSemana);
			if(Placa%2 ==0) {
				String restriccionFines = alerta.get("restriccionFines").get(1).asText();
				restricciones.add(restriccionFines);
			}
			else {
				String restriccionFines = alerta.get("restriccionFines").get(0).asText();
				restricciones.add(restriccionFines);
			}
			
			
			
			Alerta alertaUsuario = new Alerta(tipo,establecimientos, restricciones);
			return alertaUsuario;
		}
		
		
		
		
		
		
		//-----------------------------------------------------------------------------------------------------//
		/////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		
		public boolean buscar(String provincia, String canton, String distrito) {
			
			ArrayNode arregloProvincias = (ArrayNode) nodoAlertas.get("provincias"); 
			if(arregloProvincias != null) {
				int i = 0;
				while (i < arregloProvincias.get(0).size()) {
					String key = String.valueOf(i+1);
					JsonNode alertaprov = arregloProvincias.get(0);
					String Provincia = alertaprov.get(key).asText();
					if (Provincia.equals(provincia)) {
						i+=1;
						if(buscarCanton(canton, i) == true & buscarDistrito(distrito)==false ) {
							return true;
						}
						else {
							return false;
						}
					}
					else {
						i+=1;
					}
					
				}
			}
			return false;
		}
		
		
		public boolean buscarCanton(String canton, int indice) {
			ArrayNode arregloCantones = (ArrayNode) nodoAlertas.get("cantones");
			String llave = String.valueOf(indice);
			if(arregloCantones != null) {
				int j = 0;
				while (j < arregloCantones.get(0).get(llave).size()) {
					JsonNode alertacant = arregloCantones.get(0).get(llave);
					String Canton = alertacant.get(j).asText();
					if (Canton.equals(canton)) {
							return true;
						}
					else {
						j+=1;
					}			
				}
					
			}	
			return false;
		}
		
		public boolean buscarDistrito(String distrito) {
			ArrayNode arregloDistritos = (ArrayNode) nodoAlertas.get("distritos");
			if(arregloDistritos != null) {
				int i = 0;
				while (i < arregloDistritos.get(0).size()) {
					String clave = String.valueOf(i+1);
					JsonNode alertadist = arregloDistritos.get(0);
					String Distrito = alertadist.get(clave).asText();
					if (Distrito.equals(distrito)) {
							return true;
						}
					else {
						i+=1;
					}
				}	
			}
			return false;
		}
		
}
