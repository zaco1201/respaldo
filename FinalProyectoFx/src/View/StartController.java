package View;

import java.io.IOException;
import Idea.JSONParser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class StartController {
	@FXML
	private TextField NombreUsuario;
	
	@FXML
	private Label Fail;
	
	@FXML
	private void initialize() 
	{
		
	}
	
	public void changeR(ActionEvent event) throws IOException{
		Parent RegistroPadre = FXMLLoader.load(getClass().getResource("Registro.fxml"));
		Scene Registro = new Scene(RegistroPadre);
		Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
		window.setScene(Registro);
		window.show();
	}
	
	public void changeI(ActionEvent event ) throws IOException{
		int SesionActual = usuarioValido();
		if(SesionActual >= 0) {
			 FXMLLoader loader = new FXMLLoader(getClass().getResource("Display.fxml"));
	         Parent InfoPadre = loader.load();
	         Scene Info = new Scene(InfoPadre);
	         Controller Envios = loader.getController();
	         Envios.determinarSesion(SesionActual);
	         Stage window = (Stage)((Node)event.getSource()).getScene().getWindow();
				window.setScene(Info);
				window.show();
		      }
		else {
			Fail.setText("Usuario o Contrasena Incorrectos");
		}
	
	}
	
	public int usuarioValido() {
		JSONParser pars = new JSONParser();
		String Usuario = NombreUsuario.getText();
		return pars.buscarUsuarios(Usuario);
	}
}
