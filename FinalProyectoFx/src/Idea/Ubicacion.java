package Idea;


public class Ubicacion {
	private String Provincia;
	private String Canton;
	private String Distrito;
	
	public Ubicacion(String provincia, String canton, String distrito) {
		super();
		Provincia = provincia;
		Canton = canton;
		Distrito = distrito;
	}

	public String getProvincia() {
		return Provincia;
	}

	public void setProvincia(String provincia) {
		Provincia = provincia;
	}

	public String getCanton() {
		return Canton;
	}

	public void setCanton(String canton) {
		Canton = canton;
	}

	public String getDistrito() {
		return Distrito;
	}

	public void setDistrito(String distrito) {
		Distrito = distrito;
	}
	
	public String toString() {
		return "Provincia"+":"+Provincia + "," +"Canton"+":"+Canton + "," +"Distrito"+":"+Distrito;
	}

}
